import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {NavigationComponent} from './navigation/navigation.component';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductItemComponent} from './product-list/product-item/product-item.component';
import {BreadcrumbsComponent} from './breadcrumbs/breadcrumbs.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NewProductComponent} from './product-list/product-item/new-product/new-product.component';
import {ProductService} from './product-list/product.service';
import {NavigationService} from './product-list/navigation.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {NewProductReactiveFormComponent} from './product-list/product-item/new-product-reactive-form/new-product-reactive-form.component';

export const appRoutes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {
    path: 'products',
    children: [
      {path: '', component: ProductListComponent},
      {path: 'details', component: ProductItemComponent},
      {path: 'new-product', component: NewProductComponent}
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ProductListComponent,
    ProductItemComponent,
    BreadcrumbsComponent,
    HomeComponent,
    NewProductComponent,
    NewProductReactiveFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      positionClass: 'toast-top-full-width'
    })
  ],
  providers: [ProductService, NavigationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
