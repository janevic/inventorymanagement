import {Product} from './product-item/product.model';
import {Observable, of} from 'rxjs';

export class ProductService {

  products: Product[];

  constructor() {
    this.products = this.mockAllProducts();
  }

  getAllProducts(): Observable<Product[]> {
    return of(this.products);
  }

  getProductsByDepartment(department: string) {
    const productsByDepartment = [];

    this.products.forEach((product: Product) => {
      if (product.departments) {
        if (product.departments.includes(department)) {
          productsByDepartment.push(product);
        }
      }
    });

    return of(productsByDepartment);
  }

  createNewProduct(product: Product): Observable<Product> {
    this.products.push(product);
    console.log('The product was created: ', product);
    return of(product);
  }

  deleteProduct(product: Product): Observable<Product[]> {
    console.log('product for removing: ', product);
    const indexOfProduct = this.products.indexOf(product);
    if (indexOfProduct !== -1) {
      const removedItem = this.products.splice(indexOfProduct, 1);
      console.log();
    }
    return of(this.products);
  }

  private mockAllProducts(): Product[] {
    return [
      new Product(
        'Fence 1',
        850,
        2,
        ['../../../assets/imgs/fence_1.jpg', '../../../assets/imgs/fence_2.jpg'],
        ['#fence', '#outdoors']
      ),
      new Product(
        'Fence 2',
        1200,
        3,
        ['../../../assets/imgs/fence_3.jpg', '../../../assets/imgs/fence_7.jpg'],
        ['#outdoors', '#fence', '#design']),
      new Product(
        'Fence 3',
        800,
        5,
        ['../../../assets/imgs/fence_4.jpg', '../../../assets/imgs/fence_8.jpg'],
        ['#design', '#outdoors ', '#yard', '#home']),
      new Product(
        'Fence 4',
        1450,
        7,
        ['../../../assets/imgs/fence_5.jpg', '../../../assets/imgs/fence_9.jpg'],
        ['#design', '#yard', '#home']),
      new Product(
        'Fence 5',
        1800,
        1,
        ['../../../assets/imgs/fence_6.jpg', '../../../assets/imgs/fence_10.jpg']),
      new Product(
        'Fence 6',
        720,
        3,
        ['../../../assets/imgs/fence_7.jpg', '../../../assets/imgs/fence_11.jpg']),
      new Product(
        'Fence 7',
        650,
        5,
        ['../../../assets/imgs/fence_8.jpg', '../../../assets/imgs/fence_12.jpg'],
        ['#design', '#outdoors']),
      new Product(
        'Fence 8',
        550,
        4,
        ['../../../assets/imgs/fence_9.jpg', '../../../assets/imgs/fence_13.jpg']),
    ];
  }
}
