export class Department {
  constructor() {
  }

  public static getMockDepartments(): string[] {
    return ['All items', '#outdoors', '#design', '#home', '#yard', '#fence', '#house'];
  }
}
