import {Component, OnInit} from '@angular/core';
import {Department} from '../department.model';
import {ProductService} from '../../product.service';
import {Product} from '../product.model';
import {NavigationService} from '../../navigation.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  departments: string[];
  product: Product;

  constructor(
    private _productService: ProductService,
    private _navigationService: NavigationService,
    private _toast: ToastrService) {
    // this.product = new Product('', 0, 0, [], []);
    this.product = new Product('');
  }

  ngOnInit() {
    this.departments = Department.getMockDepartments();
  }

  createProduct(product: Product): void {
    // Get random image from assets
    const randomNumber = Math.floor(Math.random() * 13) + 1;
    product.imageUrls = [];
    product.imageUrls.push('../../../assets/imgs/fence_' + randomNumber + '.jpg');

    this._productService.createNewProduct(product)
      .subscribe((newProduct: Product) => {
        if (newProduct) {
          this._navigationService.goToListOfProducts()
            .catch((reason) => {
              console.log('error while creating new product');
              this._toast.error(reason);
            })
            .then((success) => {
              console.log('product created');
              this._toast.show('Product created');
            });
        }
      });
  }

  get diagnostic() { return JSON.stringify(this.product); }
}
