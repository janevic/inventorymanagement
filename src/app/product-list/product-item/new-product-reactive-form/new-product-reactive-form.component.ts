import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {NavigationService} from '../../navigation.service';
import {ProductService} from '../../product.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Department} from '../department.model';
import {Product} from '../product.model';

@Component({
  selector: 'new-product-reactive-form',
  templateUrl: './new-product-reactive-form.component.html',
  styleUrls: ['./new-product-reactive-form.component.scss']
})
export class NewProductReactiveFormComponent implements OnInit {

  allDepartments: string[];
  newProductForm: FormGroup;

  name: AbstractControl;
  price: AbstractControl;
  amount: AbstractControl;
  departments: AbstractControl;

  constructor(
    private _productService: ProductService,
    private _navigationService: NavigationService,
    private _toast: ToastrService,
    private _fb: FormBuilder) {

    this.newProductForm = _fb.group({
      name: [null, Validators.required],
      price: [null, Validators.required],
      amount: [null, Validators.required],
      departments: [null, Validators.compose([Validators.required, this.departmentsValidator])]
    });

    this.name = this.newProductForm.controls['name'];
    this.price = this.newProductForm.controls['price'];
    this.amount = this.newProductForm.controls['amount'];
    this.departments = this.newProductForm.controls['departments'];
  }

  ngOnInit() {
    this.allDepartments = Department.getMockDepartments();
  }

  departmentsValidator(control: FormControl): { [s: string]: boolean} {
    if (control.value) {
      if (control.value.length < 3 && control.value[0] !== 'All items') {
        return {invalidNumberOfSelectedDepartments: true};
      }
    }
  }

  onSubmit(product: Product): void {
    this._productService.createNewProduct(product)
      .subscribe((newProduct: Product) => {
        this._toast.success('New product created', `Created product: ${newProduct.name}`);
      });
  }
}
