export class Product {

  name: string;
  price: number;
  amount: number;
  imageUrls: string[];
  departments: string[];

  constructor(name: string, price?: number, amount?: number, imageUrls?: string[], department?: string[]) {
    this.name = name;
    this.price = price;
    this.amount = amount;
    this.imageUrls = imageUrls;
    this.departments = department;
  }

  // constructor() {
  //
  // }
}
