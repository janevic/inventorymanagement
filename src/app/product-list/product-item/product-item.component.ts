import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from './product.model';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent {

  @Input() product: Product;
  @Output() departmentParameter: EventEmitter<string> = new EventEmitter<string>();
  @Output() deleteItem: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  emitDepartmentParam(departmentParam: string): void {
    this.departmentParameter.emit(departmentParam);
  }

  deleteProduct() {
    this.deleteItem.emit(true);
  }

}
