import {Router} from '@angular/router';
import {Injectable} from '@angular/core';

@Injectable()
export class NavigationService {

  constructor(private _router: Router) {

  }

  goToListOfProducts(): Promise<boolean> {
    return this._router.navigateByUrl('/products');
  }
}
