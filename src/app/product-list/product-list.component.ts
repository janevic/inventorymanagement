import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from './product.service';
import {Subscription} from 'rxjs';
import {Product} from './product-item/product.model';
import {Department} from './product-item/department.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {

  products: Product[];
  departments: string[];
  private _getProductsSubscription: Subscription;
  private _deleteProductSubscription: Subscription;

  constructor(private _productService: ProductService) {
  }

  ngOnInit() {
    this.departments = Department.getMockDepartments();
    this.products = [];
    this.getAllProducts();
  }

  ngOnDestroy() {
    this._unsibscribe(this._getProductsSubscription);
    this._unsibscribe(this._deleteProductSubscription);
  }

  getAllProducts(): void {
    this._getProductsSubscription = this._productService.getAllProducts()
      .subscribe((products: Product[]) => {
        this.products = products;
      });
  }

  getProductsForDepartment(departmentParam: string): void {
    this.products = [];
    if (departmentParam !== 'All items') {
      this._getProductsSubscription = this._productService.getProductsByDepartment(departmentParam)
        .subscribe((products: Product[]) => {
          this.products = products;
        });
    } else {
      this.getAllProducts();
    }
  }

  deleteItem(product: Product) {
    this._deleteProductSubscription = this._productService.deleteProduct(product)
      .subscribe((newList: Product[]) => {
        this.products = newList;
      });
  }

  private _unsibscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
    }
  }
}
